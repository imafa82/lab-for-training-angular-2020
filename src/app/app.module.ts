import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {NavComponent} from './components/nav/nav.component';
import { FooterComponent } from './components/footer/footer.component';
import { ShoppingListComponent } from './components/shopping-list/shopping-list.component';
import { ImageCategoryComponent } from './components/image-category/image-category.component';
import { HomeComponent } from './components/home/home.component';
import { PipeViewComponent } from './components/pipe-view/pipe-view.component';
import { UsersComponent } from './components/users/users.component';
import { HttpClientModule} from '@angular/common/http';
import { ListUsersComponent } from './components/users/list-users/list-users.component';
import { UserDetailsComponent } from './components/users/user-details/user-details.component';
import { CustomCardComponent } from './components/shared/design/bootstrap/custom-card/custom-card.component';
import { ListIcoComponent } from './components/shared/design/list-ico/list-ico.component';
import { AddressInfoComponent } from './components/users/user-details/address-info/address-info.component';
import { GeneralInfoComponent } from './components/users/user-details/general-info/general-info.component';
import { ProfileInfoComponent } from './components/users/user-details/profile-info/profile-info.component';
import { InfoCustomComponent } from './components/shared/design/info-custom/info-custom.component';
import { WeatherComponent } from './components/shared/weather/weather.component';
import { WeatherPageComponent } from './components/weather-page/weather-page.component';
import { DynamicStyleComponent } from './components/dynamic-style/dynamic-style.component';
import { ObjectKeyPipe } from './pipes/object-key.pipe';
import { SingleStyleComponent } from './components/dynamic-style/single-style/single-style.component';
import { FilterArrayPipe } from './pipes/filter-array.pipe';
import { ElementListComponent } from './components/shared/design/element-list/element-list.component';
import {ObjectUtils} from './components/shared/utils/objectUtils';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FooterComponent,
    ShoppingListComponent,
    ImageCategoryComponent,
    HomeComponent,
    PipeViewComponent,
    UsersComponent,
    ListUsersComponent,
    UserDetailsComponent,
    CustomCardComponent,
    ListIcoComponent,
    AddressInfoComponent,
    GeneralInfoComponent,
    ProfileInfoComponent,
    InfoCustomComponent,
    WeatherComponent,
    WeatherPageComponent,
    DynamicStyleComponent,
    ObjectKeyPipe,
    SingleStyleComponent,
    FilterArrayPipe,
    ElementListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ObjectUtils],
  bootstrap: [AppComponent]
})
export class AppModule { }
