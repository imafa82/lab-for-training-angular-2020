import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-weather-page',
  templateUrl: './weather-page.component.html',
  styleUrls: ['./weather-page.component.scss']
})
export class WeatherPageComponent implements OnInit {
  city: string;
  test: string;
  constructor() {
    this.city = 'Rome';
    this.test = 'Iniziale';
    setTimeout(() => {
      this.test = 'Finale';
    }, 5000);
  }

  ngOnInit(): void {
  }


  setCity(form: NgForm){
    if(form.valid){
      this.city = form.value.city;
    }
  }

}
