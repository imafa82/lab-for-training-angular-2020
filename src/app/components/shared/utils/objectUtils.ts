export class ObjectUtils {
    lastObject: any;
    isFull(obj: any): boolean{
        return this.isObject(obj) && !!Object.keys(obj).length;
    }

    isEmpty(obj: any){
        return !this.isFull(obj);
    }

    isObject(obj): boolean{
        this.lastObject = obj;
        return typeof obj === 'object' && obj !== null && obj.constructor === Object;
    }
}
