export interface IcoCustom {
    ico: string;
    action: () => void;
}
