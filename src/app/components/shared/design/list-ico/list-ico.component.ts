import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IcoCustom} from './models/IcoCustom';

@Component({
  selector: 'app-list-ico',
  templateUrl: './list-ico.component.html',
  styleUrls: ['./list-ico.component.scss']
})
export class ListIcoComponent implements OnInit {
  @Input() ico: string | IcoCustom[];
  @Output() action: EventEmitter<IcoCustom> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  icoAction(ico: IcoCustom){
    this.action.emit(ico);
  }

  getIcoList(){
    return typeof this.ico === 'string' ? [{ico: this.ico, action: ''}] : this.ico;
  }
}
