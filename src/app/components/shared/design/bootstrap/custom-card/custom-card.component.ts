import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IcoCustom} from '../../list-ico/models/IcoCustom';

@Component({
  selector: 'app-custom-card',
  templateUrl: './custom-card.component.html',
  styleUrls: ['./custom-card.component.scss']
})
export class CustomCardComponent implements OnInit {
  @Input() title: string;
  @Input() ico: string | IcoCustom[];
  @Output() action: EventEmitter<IcoCustom> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  icoAction(ico: IcoCustom){
    this.action.emit(ico);
  }
}
