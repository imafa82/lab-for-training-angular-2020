import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WeatherApi} from './models/WeatherApi';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit, OnChanges {
  weather: WeatherApi;
  @Input() city: string;
  @Input() test: string;
  constructor(private http: HttpClient) {
    this.city = 'Rome';
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.city){
      this.http.get<WeatherApi>('http://api.weatherstack.com/current?access_key=3f06917a4b742dbbe5eda9b0880ea454&query=' + this.city)
          .subscribe(res => {
            this.weather = res;
          });
    }
  }
}
