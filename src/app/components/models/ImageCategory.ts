export interface ImageCategory {
    category: string;
    label: string;
}
