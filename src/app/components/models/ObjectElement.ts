export interface ObjectElement {
    key: string;
    value: string;
}
