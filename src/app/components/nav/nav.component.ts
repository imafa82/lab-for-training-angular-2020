import {Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from '../models/User';
import {VoiceMenu} from '../models/Menu';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss']
})
export class NavComponent {
    @Input() userLogged: User;
    @Input() voices: VoiceMenu[];
    @Output() selected: EventEmitter<string> = new EventEmitter();
    selectedView(view: string){
        this.selected.emit(view);
    }
}
