import { Component, OnInit } from '@angular/core';
import {ImageCategory} from '../models/ImageCategory';

@Component({
  selector: 'app-image-category',
  templateUrl: './image-category.component.html',
  styleUrls: ['./image-category.component.scss']
})
export class ImageCategoryComponent implements OnInit {
  categories: ImageCategory[];
  selectedCategory: string;
  constructor() {
    setTimeout(()=> {
      this.categories = [
        {
          category: 'abstract',
          label: 'Astratti'
        },
        {
          category: 'animals',
          label: 'Animali'
        },
        {
          category: 'business',
          label: 'Lavoro'
        },
        {
          category: 'cats',
          label: 'Gatti'
        },
        {
          category: 'city',
          label: 'Città'
        },
        {
          category: 'transport',
          label: 'Trasporti'
        }
      ];
      this.selectedCategory = this.categories[0].category;
    }, 2000);
  }

  ngOnInit(): void {
  }

  setUrl(category) : void{
    this.selectedCategory = category.category;
  }
  getUrl(): string{
    return `https://loremflickr.com/320/240/${this.selectedCategory}`;
  }

}
