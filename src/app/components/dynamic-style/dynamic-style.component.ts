import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ObjectElement} from '../models/ObjectElement';

@Component({
  selector: 'app-dynamic-style',
  templateUrl: './dynamic-style.component.html',
  styleUrls: ['./dynamic-style.component.scss']
})
export class DynamicStyleComponent implements OnInit {
  stylePage: any;
  constructor() {
    this.stylePage = {
      backgroundColor: 'red',
      width: '500px',
      height: '500px'
    };
  }

  ngOnInit(): void {
  }

  addStyle(form: NgForm){
    const {property, value } = form.value;
    this.stylePage[property] = value;
    this.stylePage = {...this.stylePage};
    form.reset();
  }

  setChangeStyle(element: ObjectElement){
    this.stylePage[element.key] = element.value;
  }


  removeStyle(el){
    this.stylePage = Object.keys(this.stylePage).reduce((obj, key) => {
      if(key !== el){
        obj[key] = this.stylePage[key];
      }
      return obj;
    }, {});
    // delete this.stylePage[el];
    // this.stylePage = stylePage;
  }
}
