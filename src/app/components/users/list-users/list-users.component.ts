import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserApi} from '../../models/UserApi';
import {ObjectUtils} from '../../shared/utils/objectUtils';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss'],
  providers: [ObjectUtils]
})
export class ListUsersComponent implements OnInit {
  @Input() users: UserApi[];
  search: string;
  @Output() select: EventEmitter<UserApi> = new EventEmitter();
  constructor(private objectUtils: ObjectUtils) {
    console.log(this.objectUtils.lastObject);
  }

  ngOnInit(): void {
  }

  setUser(user: UserApi){
    this.select.emit(user);
  }

}
