import {Component, Input, OnInit} from '@angular/core';
import {UserApi} from '../../../models/UserApi';

@Component({
  selector: 'app-general-info',
  templateUrl: './general-info.component.html',
  styleUrls: ['./general-info.component.scss']
})
export class GeneralInfoComponent implements OnInit {
  @Input() user: UserApi;
  constructor() { }

  ngOnInit(): void {
  }

}
