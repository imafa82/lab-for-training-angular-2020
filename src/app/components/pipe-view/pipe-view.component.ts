import { Component, OnInit } from '@angular/core';
import {ObjectUtils} from '../shared/utils/objectUtils';

@Component({
  selector: 'app-pipe-view',
  template: `
        <p>Oggi è: {{today | date:'dd MMMM yyyy'}}</p>
        <p>Importo: {{import | currency:'EUR'}}</p>
        <p>Number: {{number | number: '2.2-4'}}</p>
        <p>Oggetto: {{obj | json}}</p>
  `,
  styleUrls: ['./pipe-view.component.scss']
})
export class PipeViewComponent implements OnInit {
  today = Date.now();
  import = 12434534;
  number = 1233.3;
  obj = {
    name : 'Massimiliano',
    surname: 'Salerno'
  }
  constructor(private objectUtils: ObjectUtils) {

  }

  ngOnInit(): void {
    console.log(this.objectUtils.lastObject);
    console.log(this.objectUtils.isObject({test: 'prova'}), 'pipe view');
  }

}
