import { Component, OnInit } from '@angular/core';
import {ObjectUtils} from '../shared/utils/objectUtils';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title: string;
  number1: number = 2;
  number2: number = 4;
  gender: 'F' | 'M'  = 'F';
  // objectUtils: ObjectUtils;

  constructor(public objectUtils: ObjectUtils) {
    this.title = 'La nostra prima applicazione';
    // this.objectUtils = new ObjectUtils();
    // this.objectUtils = objectUtils;

  }

  ngOnInit(): void {
    let obj = {};
    let arr = [];
    console.log(this.objectUtils.isObject(obj));
    console.log(this.objectUtils.isObject(arr));
    console.log(this.objectUtils.lastObject);
  }
  setGender() {
    this.gender = this.gender === 'F' ? 'M' : 'F';
  }


  getTitle(){
    return this.title.toUpperCase();
  }
}
