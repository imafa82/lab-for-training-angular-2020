import { Component, OnInit } from '@angular/core';
import {ShoppingService} from '../../services/shopping.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss'],
  providers: [ShoppingService]
})
export class ShoppingListComponent implements OnInit {
  openCard: boolean;
  list: string[];
  search: string;

  constructor(private shoppingService: ShoppingService) {
    this.openCard = true;
    this.list = this.shoppingService.getList();
  }

  ngOnInit(): void {
  }

  addElement(el){
    if(el.target.value !== ''){
      // this.list.push(el.target.value);
      this.list = [...this.list, el.target.value];
      el.target.value = '';
    }
  }

  removeElement(ele: string){
      this.list = this.list.filter(element => element !== ele);
  }

}
