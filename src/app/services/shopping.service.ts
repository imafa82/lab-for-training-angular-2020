import { Injectable } from '@angular/core';

@Injectable()
export class ShoppingService {
  list: string[];
  constructor() {
    this.init();
  }

  init(){
    this.list = [ 'frutta', 'verdura', 'patate' ];
  }

  getList(){
    return this.list;
  }
}
