import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectKey'
})
export class ObjectKeyPipe implements PipeTransform {

  transform(value: any, keyValue: boolean = false): any[] {
    return value && Object.keys(value) ? this.switchKeyValue(value, keyValue) : [];
  }

  switchKeyValue(value, keyValue){
    return keyValue ? this.calcObj(value) : Object.keys(value);
  }

  calcObj(value){
    return Object.keys(value).map(key => ({key, value: value[key]}));
  }

}
