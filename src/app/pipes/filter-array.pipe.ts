import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterArray'
})
export class FilterArrayPipe implements PipeTransform {

  transform(value: any[], search: string = '', property?: string | string[]): any[] {
    let result = [];
    if(value && value.length){
      result = value.filter(el => {
          return property ? this.findObj(el, property, search) : this.findText(el, search);
      });
    }
    return result;
  }

  findObj(obj, property, search){
    return typeof property === 'string' ?
      this.findSingleElementObj(obj, property, search) :
      this.findElementsObj(obj, property, search);
  }

  findText(el, search){
    return el.toLowerCase().trim().indexOf(search.toLowerCase().trim()) !== -1;
  }

  findSingleElementObj(obj, property, search){
    return this.findText(obj[property], search);
  }

  findElementsObj(obj, property, search){
    let control = false;
    property.forEach(el => {
      control = this.findSingleElementObj(obj, el, search) ? true : control;
    });
    return control;
  }

}
