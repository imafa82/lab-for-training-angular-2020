import { Component } from '@angular/core';
import {User} from './components/models/User';
import {VoiceMenu} from './components/models/Menu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  userLogged: User;
  voicesMenu: VoiceMenu[];
  selectedView: string;
  container: string;
  constructor() {
    this.userLogged = {
      name: 'Massimiliano'
    };
    this.voicesMenu = [
      {
        label: 'Home',
        view: 'home'
      },
      {
        label: 'Categorie',
        view: 'category'
      },
      {
        label: 'Lista spesa',
        view: 'shopping'
      },
      {
        label: 'Pipe',
        view: 'pipe'
      },
      {
        label: 'Utenti',
        view: 'users'
      },
      {
        label: 'Meteo',
        view: 'weather'
      },
      {
        label: 'Stile dinamico',
        view: 'dynamic'
      }
    ];
    this.selectedView = this.voicesMenu[0].view;
    this.setContainer();
  }
  setSelectedView(view: string){
      this.selectedView = view;
      this.setContainer();
  }
  setContainer(bool: boolean = false){
      this.container = bool ? 'container-fluid' : 'container';
  }

}
